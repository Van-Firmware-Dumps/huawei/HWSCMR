#!/bin/sh
#######################################################################
##- Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
##- @Description: start isulad
##- @Author: liyadong
##- @Create: 2022-06-06
#######################################################################*/
# set -e

# remove old link file
rm -f /mnt/isula/var
# remove old config of container and isulad,
# ensure every reboot is new
rm -rf /data/isula/var

SHELL_LOG="/data/log/hsl_share/HSL_isulad.sh.log"

# wait log hsl_share dir is ready
while [ ! -e /data/log/hsl_share ]; do
    sleep 1
done
# clear old log
echo "begin isulad.sh..." > $SHELL_LOG
chmod 644 $SHELL_LOG

mkdir -p /dev/hsl/run
echo "mkdir hsl run: $?" >> $SHELL_LOG
restorecon -RF /dev/hsl
echo "restorecon dev hsl: $?" >> $SHELL_LOG

mkdir -p /mnt/isula
echo "mkdir mnt isula: $?" >> $SHELL_LOG
# wait hsl_proxy.sh created /data/isula dir
while [ ! -e /data/isula ]; do
    sleep 1
done
restorecon -RF /data/isula
echo "restorecon data isula: $?" >> $SHELL_LOG
# create after change selinux label, to ensure have permission
# and must create before link to /mnt/isula/var
mkdir -p /data/isula/var
echo "mkdir data isula var: $?" >> $SHELL_LOG
# create link to new /data/isula/var
ln -sf /data/isula/var /mnt/isula/var
echo "link mnt isula to data: $?" >> $SHELL_LOG

# isulad call binary in /hw_product/bin/isula/bin, require set PATH to /hw_product/bin/isula/bin
export PATH=/hw_product/bin/isula/bin:$PATH
# isulad depends library under /hw_product/bin/isula/lib
export LD_LIBRARY_PATH=/hw_product/bin/isula/lib:$LD_LIBRARY_PATH

echo "finish isulad.sh..." >> $SHELL_LOG
exec isulad
